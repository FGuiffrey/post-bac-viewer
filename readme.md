# post-bac-viewer

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `npm install` first.

Run `grunt` for building and `grunt serve` for preview.

Run `bower install` if you have this error :

`Cannot find where you keep your Bower packages`

## Testing

Running `grunt test` will run the unit tests with karma.
