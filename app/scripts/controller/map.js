'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.controller:MapCtrl
 * @description
 * # MapCtrl
 * Controller of the postBacViewerApp
 */
postBacViewerApp.controller('MapCtrl', ['$scope', 'csvWarehouse', 'NgMap', 'Geocoder', function ($scope, warehouse, NgMap, Geocoder) {
  NgMap.getMap().then(function(map) {
      //Init of the map
      map.setCenter({lat: 45.833619, lng: 1.261105});
      map.setZoom(15);

      // console.log('markers', map.markers);
      // console.log('shapes', map.shapes);
  });

  var self = this
  this.data = warehouse.lists;
  this.current = warehouse.current;
  
  $scope.positions = ['Rien']
  // stock et informe quand les requêtes sont terminées 
  $scope.positionLoaded = false
  
  this.getAllPosition = function (list) {
    if (_.isEmpty(list)) return []
    // console.log('list', list, typeof list)
    // @info, récupère toute les adresses complète
    var adresses = _.map(list, function (UAI) {
      console.log('UAI', UAI)
      return `${UAI[0]['Libellé établissement']} ${UAI[0]['Commune établissement']}`
    })
    // console.log('adresses', adresses)
    // Exécute toutes les requètes sans surcharger
    Promise.map(adresses, function (adresse) {
      return self.getPosition(adresse)
    }).then(
      function (result) {
        // Ne retourne rien et je comprend pas pourquoi
        $scope.positions = result
        console.log('finish', self.positions)
        $scope.positionLoaded = true
        $scope.$apply()
        // return result
      })
    }
    
    this.getPosition = function (adresse) {
      // Transforme l'adresse en position
      return Geocoder.geocodeAddress(adresse).then(
      function (result) {
        // console.log('result', result.lat, result.lng)
        return [result.lat, result.lng]
      },
      function (err) {
        console.error(err.message)
      });
    }

    $scope.$watch(self.current, function (value) {
      if (self.current && !_.isEmpty(self.current)) {
        self.getAllPosition(_.get(self.current, 'grouped.highSchool', []))
      }
    })

    this.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlhedQLNeRS0DiRb2rrk13jQTn6H-PILo";
  }]);
