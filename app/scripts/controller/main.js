'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the postBacViewerApp
 */
postBacViewerApp.controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
