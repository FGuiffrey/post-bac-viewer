'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the postBacViewerApp
 */
postBacViewerApp.controller('AdminCtrl', ['$scope', 'csvWarehouse', function ($scope, warehouse) {

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    this.infos = adminInfos;

    var self = this
    this.data = warehouse.lists;
    this.current = warehouse.current;

    this.setCurrent = function(csv) {
      self.current = csv
      warehouse.setCurrent(csv)
    };

    this.changeName = function(csv) {
      csv.name = csv;
    };

    this.fileDetected = function(el) {
      // warehouse.getFiles(el.files[0], self.data)
      Papa.parse(el.files[0], {
        delimiter: ';',
        header: true,
        encoding: 'ISO-8859-14',
        complete: function (result, thefile) {
          console.log('complete', result)
          warehouse.files.push(thefile)
          warehouse.lists.push({
            name: thefile.name,
            nbCandidatures: result.data.length,
            content: result.data,
            filtered: {
              // Filtres : M&F; Boursier or not bourse;
              H: _.filter(result.data, function (row) { return row.Sexe === 'M' }),
              F: _.filter(result.data, function (row) { return row.Sexe === 'F' })
            },
            grouped: {
              highSchool: _.groupBy(result.data, 'UAI établissement')
            }
          });
          console.log('self.data', self.data)
          warehouse.current = warehouse.lists[0]
          $scope.$apply()
          // data = warehouse.lists
        }
      });
    }
  }]);

  //Object infos
  var adminInfos = {
    title : 'Administration',
    class : 'js-admin-view'
  }
