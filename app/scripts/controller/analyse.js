'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.controller:AnalyseCtrl
 * @description
 * # AnalyseCtrl
 * Controller of the postBacViewerApp
 */
postBacViewerApp.controller('AnalyseCtrl', ['$scope', 'csvWarehouse', function ($scope, warehouse) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    /* Gestion CSV */
    var self = this
    this.data = warehouse.lists;
    this.current = warehouse.current;

    this.setCurrent = function(csv) {
      self.current = csv
      warehouse.setCurrent(csv)
    };

    this.infos = analyseInfos;
    this.currentType = 'geo';
    this.currentClass = 'analyse-view--' + this.currentType;

    this.setCurrentType = function() {
      if(this.currentType === 'graph')
        this.currentType = 'geo';
      else
        this.currentType = 'graph';

      this.setCurrentClass(this.currentType)
    }
    
    this.setCurrentClass = function(currentType) {
      this.currentClass = 'analyse-view--' + currentType;
    }
  }]);

  //Object infos
  var analyseInfos = {
    title : 'Analyse',
    class : 'js-analyse-view'
  }