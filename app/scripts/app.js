'use strict';

/**
 * @ngdoc overview
 * @name postBacViewerApp
 * @description
 * # postBacViewerApp
 *
 * Main module of the application.
 */
var postBacViewerApp = angular
.module('postBacViewerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMap',
    'geocoder'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/analyse'
      })
      .when('/analyse', {
        templateUrl: 'views/page/analyse.html',
      })
      .when('/admin', {
        templateUrl: 'views/page/administration.html',
      })
      .otherwise({
        redirectTo: '/'
      });
  });

