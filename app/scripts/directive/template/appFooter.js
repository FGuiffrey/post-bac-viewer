'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.directive:appFooter
 * @description
 * # appFooter
 * Directive of the postBacViewerApp
 */
postBacViewerApp.directive('appFooter', function(){
  return{
    restrict: 'E',
    templateUrl: 'views/directive/app-footer.html'
  };
});