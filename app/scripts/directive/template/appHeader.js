'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.directive:appHeader
 * @description
 * # appHeader
 * Directive of the postBacViewerApp
 */
postBacViewerApp.directive('appHeader', function(){
  return{
    restrict: 'E',
    templateUrl: 'views/directive/app-header.html'
  };
});