'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.directive:analyseGraph
 * @description
 * # analyseGraph
 * Directive of the postBacViewerApp
 */
postBacViewerApp.directive('analyseGraph', function(){
  return{
    restrict: 'E',
    templateUrl: 'views/directive/analyse-graph.html'
  };
});