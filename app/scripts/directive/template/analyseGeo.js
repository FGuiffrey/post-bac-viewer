'use strict';

/**
 * @ngdoc function
 * @name postBacViewerApp.directive:analyseGeo
 * @description
 * # analyseGeo
 * Directive of the postBacViewerApp
 */
postBacViewerApp.directive('analyseGeo', function(){
  return{
    restrict: 'E',
    templateUrl: 'views/directive/analyse-geo.html'
  };
});