postBacViewerApp.directive('selectric', ['$timeout', function($timeout) {
    return {
        link: function(scope, element, attrs) {
            $timeout(function() {
                $(element).selectric();
            });
        }
    }
}]);